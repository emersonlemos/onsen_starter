var app = ons.bootstrap('starter', ['onsen']).run(function ($rootScope) {

    ons.ready(function () {

        //back
        $rootScope.back = function () {
            nav.popPage({
                refresh: true
            });
        };

        //forward
        $rootScope.forward = function (page) {
            nav.pushPage('templates/' + page + '.html');
        };

        //go to first page
        $rootScope.forward(1);

    });
});