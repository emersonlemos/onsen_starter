app.directive("floatspan", function ($timeout) {
    return {
        restrict: 'A',
        link: function ($scope, element) {
            var t = $timeout(function () {
                element.css("line-height", element.parent().children()[0].clientHeight + "px");
                $timeout.cancel(t);
            }, 500);
        }
    };
});

app.directive("infinitescroller", function ($rootScope, $timeout, sharedData, $interval) {
    return {
        restrict: 'E',
        template: '<div class="iconCenter"><svg class="progress-circular"><circle class="progress-circular__primary" cx="50%" cy="50%" r="25%" fill="none" stroke-width="5%" stroke-miterlimit="20"/></svg></div>',
        link: function ($scope, element) {
            var page = element.parent().parent();
            var isLoading = false;
            page.on("scroll", function () {
                if (!isLoading && $scope.filtered.length > $rootScope.limitNumber) {
                    if (page[0].scrollTop >= (page[0].scrollHeight - page[0].clientHeight) - 50) {
                        isLoading = true;
                        var t = $timeout(function () {
                            $rootScope.limitNumber = $rootScope.limitNumber + 50;
                            $timeout.cancel(t);
                            isLoading = false;
                        }, 1000);
                    }
                }
                if (page[0].scrollTop > 300) {
                    angular.element(document.getElementsByClassName("toTop")[0]).addClass("toTopShow");
                } else {
                    angular.element(document.getElementsByClassName("toTop")[0]).removeClass("toTopShow");
                }
            });

            $scope.$on("getscrollpos", function () {
                sharedData.setScrollPos(page[0].scrollTop);
            });

            $scope.$on("setscrollpos", function () {
                page[0].scrollTop = sharedData.getScrollPos();
            });

            $scope.$on('scrolltotop', function () {
                if (page[0].scrollTop !== 0) {
                    var i = $interval(function () {
                        page[0].scrollTop = page[0].scrollTop - 1000;
                        if (page[0].scrollTop === 0) {
                            $interval.cancel(i);
                        }
                    }, 1);
                }
            });
        }
    };
});