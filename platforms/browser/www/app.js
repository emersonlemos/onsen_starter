var app = ons.bootstrap('alarmlookup', ['onsen', 'ngSanitize']).run(function ($rootScope, sharedData, alarmRequest, $http, $window, $timeout) {

    ons.ready(function () {

        //disable back button
        ons.disableDeviceBackButtonHandler();
        document.addEventListener('backbutton', function () {}, false);

        //online listener
        $rootScope.online = true;
        document.addEventListener("offline", function () {
            $rootScope.online = false;
        }, false);

        document.addEventListener("online", function () {
            $rootScope.online = true;
        }, false);

        //Google Analytics
        if (window.analytics) {
            window.analytics.startTrackerWithId('UA-66562203-2');
            window.analytics.trackView('App launch');
        }

        //set previous location to false
        $rootScope.prevLocation = false;

        //popovers
        $rootScope.changeView = null;
        $rootScope.langPopover = null;
        ons.createPopover('templates/popovers/lang.html').then(function (popover) {
            $rootScope.langPopover = popover;
            $rootScope.$digest();
        });

        //back
        $rootScope.back = function () {
            nav.popPage({
                refresh: true
            });
        };

        //forward
        $rootScope.forward = function (page) {
            nav.pushPage('templates/' + page + '.html');
        };

        //get settings object
        if ($window.localStorage.getItem("settingsObj") === null) {
            $rootScope.settingsObj = {
                pIndex: null,
                vIndex: null,
                lIndex: null
            };
        } else {
            $rootScope.settingsObj = JSON.parse($window.localStorage.getItem("settingsObj"));
            $rootScope.prodIndex = $rootScope.settingsObj.pIndex;
            $rootScope.verIndex = $rootScope.settingsObj.vIndex;
            $rootScope.langIndex = $rootScope.settingsObj.lIndex;
        }

        //save settings object
        $rootScope.saveSettings = function (p, v, l) {
            $rootScope.settingsObj = {
                pIndex: p,
                vIndex: v,
                lIndex: l
            };
            $window.localStorage.setItem('settingsObj', JSON.stringify($rootScope.settingsObj));
        };

        //master object
        $rootScope.masterObj = null;
        if ($window.localStorage.getItem("registration") === null || $window.localStorage.getItem("settingsObj") === null) {
            $rootScope.forward("registration");
        } else {
            $rootScope.masterObj = JSON.parse($window.localStorage.getItem("registration"));
            $rootScope.forward("search");
            $rootScope.$digest();
        }

        //user info
        $rootScope.userInfo = null;
        if ($window.localStorage.getItem("userInfo") === null) {
            $rootScope.userInfo = {
                name: null,
                email: null,
                company: null,
                role: null
            };
        } else {
            $rootScope.userInfo = JSON.parse($window.localStorage.getItem("userInfo"));
        }

        //search toggles
        if ($window.localStorage.getItem("searchFilters") !== null) {
            $rootScope.searchFilters = JSON.parse($window.localStorage.getItem("searchFilters"));
        } else {
            $rootScope.searchFilters = {
                optia: [{
                    name: "prompt",
                    value: "Prompt",
                    checked: false
            }, {
                    name: "safety",
                    value: "_",
                    checked: false
            }],
                trima: [{
                    name: "safety",
                    value: "<Safety>",
                    checked: false
            }]
            };
        }

        //limit number of alarm returns
        $rootScope.limitNumber = 50;

        //requesting alarms
        $rootScope.requestedLangIndex = null;
        $rootScope.requestedAlarmNames = null;

        //function to see if the array contains an object
        $rootScope.containsObject = function (obj, list) {
            for (var i = 0; i < list.length; i++) {
                if (angular.equals(list[i], obj)) {
                    return true;
                }
            }
            return false;
        };

        //watch for resizing title in alarm view
        $rootScope.currentAlarm = null;
        $rootScope.$watch("currentAlarm", function (n) {
            if (n) {
                var t = $timeout(function () {
                    var headerHeight = document.getElementsByClassName("alarmHeader")[0].clientHeight + 20 + "px";
                    angular.element(document.getElementsByClassName("tabBar")[0]).css("top", headerHeight);
                    $timeout.cancel(t);
                }, 50);
            }
        });

        //change language function
        $rootScope.changeLang = function (l, isAlarms) {

            if (!l) {
                return;
            }

            //set language index
            $rootScope.langIndex = l.idx;

            //reset search val
            sharedData.setSearchVal('');

            //if it is an alarm, request it
            if (isAlarms) {
                alarmRequest.request().then(function (response) {
                    $rootScope.currentAlarm = response;
                });

            } else {

                //if it is the alarm list
                $http.get("data/" + $rootScope.masterObj[$rootScope.prodIndex].shortName + "/" + $rootScope.masterObj[$rootScope.prodIndex].versions[$rootScope.verIndex].shortName + "/" + $rootScope.masterObj[$rootScope.prodIndex].versions[$rootScope.verIndex].shortName + "_alarmNames_" + l.langCode + ".json").success(function (response) {
                    $rootScope.alarmNames = response.nameList;
                });
            }
        };

    });
});